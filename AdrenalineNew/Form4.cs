﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdrenalineNew
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            //TopMost = true;
            InitializeComponent();
            this.KeyPreview = true;

        }

        private void label7_MouseEnter(object sender, EventArgs e)
        {
            label7.ForeColor = Color.FromArgb(33, 33, 220);
        }

        private void label7_MouseLeave(object sender, EventArgs e)
        {
            label7.ForeColor = Color.FromArgb(33, 33, 255);
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            label1.ForeColor = Color.FromArgb(33, 33, 220);
        }

        private void label1_MouseLeave(object sender, EventArgs e)
        {
            label1.ForeColor = Color.FromArgb(33, 33, 255);
        }

        private void label7_Click(object sender, EventArgs e)
        {
            AdrenalineNew.Channel = true;
            this.Close();
        }


        private void label1_Click(object sender, EventArgs e)
        {
            this.Close();
        }



    }
}
