﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdrenalineNew
{
   
    public partial class Form2 : Form
    {
        public Form2()
        {
            TopMost = true;
            InitializeComponent();
            this.KeyPreview = true;

        }

        private void label7_MouseEnter(object sender, EventArgs e)
        {
            label7.ForeColor = Color.FromArgb(33,33,220);
        }

        private void label7_MouseLeave(object sender, EventArgs e)
        {
            label7.ForeColor = Color.FromArgb(33, 33, 255);
        }

        private void label7_Click(object sender, EventArgs e)
        {
            AdrenalineNew.Path = textBox1.Text;
            this.Close();
        }

        private void Form2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) label7_Click(null, null);
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }


    }
}
