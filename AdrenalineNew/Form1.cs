﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Threading;
using System.Text.RegularExpressions;

namespace AdrenalineNew
{
    public partial class AdrenalineNew : Form
    {
        //Для подключения к чату
        JObject o;
        WebClient c = new WebClient();
        static public string Path = "http://chat.sc2tv.ru/memfs/channel-666.json";
        //
        int Count = 0;
        string Last = "";
        
        bool Connected = false;
        public AdrenalineNew()
        {
            InitializeComponent();

            CheckForIllegalCrossThreadCalls = false;
            backgroundWorker2.RunWorkerAsync();
            dataGridView1.Rows.Add("Connecting...");
        }

        private void panel1_MouseWheel(object sender, MouseEventArgs e)
        {
            if (e.Delta > 0 && dataGridView1.FirstDisplayedScrollingRowIndex > 0)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex--;
            }
            else if (e.Delta < 0)
            {
                dataGridView1.FirstDisplayedScrollingRowIndex++;
            }
        }

        #region Верхняя панель
        Point loc = new Point();

        [DllImport("user32")]
        public static extern void SendMessageA(IntPtr hWnd, int wMsg, int wParam, int lParam);
        [DllImport("user32")]
        public static extern void ReleaseCapture();

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HTCAPTION = 2;





        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessageA(this.Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }




        private void label1_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }

        private void panel2_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }


        private void panel2_MouseLeave(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(150, 33, 33);
        }


        private void label1_MouseLeave(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(150, 33, 33);
        }

        private void label1_MouseEnter(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(130, 33, 33);
        }

        private void panel2_MouseEnter(object sender, EventArgs e)
        {
            panel2.BackColor = Color.FromArgb(130, 33, 33);
        }

        #endregion



        void Contain(string s)
        {

            bool Contain = false;
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {

                if (s == dataGridView1.Rows[i].Cells[0].Value.ToString())
                {
                    Contain = true;
                    break;
                }

            }

            if (!Contain)
            {
                dataGridView1.Rows.Insert(0, s);
                Count++;
                label5.Text = Count.ToString();
            }

        }

        private void dataGridView1_SelectionChanged(object sender, EventArgs e)
        {
            dataGridView1.ClearSelection();
        }



        #region Ресайз окна

        Size minSize = new Size(225, 300);


        bool CheckSizeW(Size newSize)
        {
            if (minSize.Width > newSize.Width) return false;
            return true;
        }
        bool CheckSizeH(Size newSize)
        {
            if (minSize.Height > newSize.Height) return false;
            return true;
        }
    

        private void panel4_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Size newlocation = this.Size;
                newlocation.Height += e.Y - loc.Y;
                if (CheckSizeW(newlocation)) this.Size = new Size(newlocation.Width, Size.Height);
                if (CheckSizeH(newlocation)) this.Size = new Size(Size.Width, newlocation.Height);
            }
        }

        private void panel7_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Size newlocation = this.Size;
                newlocation.Width += e.X - loc.X;
                newlocation.Height += e.Y - loc.Y;
                if (CheckSizeW(newlocation)) this.Size = new Size(newlocation.Width, Size.Height);
                if (CheckSizeH(newlocation)) this.Size = new Size(Size.Width, newlocation.Height);
            }
        }

        private void panel3_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Size newlocation = this.Size;
                newlocation.Width += e.X - loc.X;
                //newlocation.Height += e.Y - loc.Y;
                if (CheckSizeW(newlocation)) this.Size = new Size(newlocation.Width, Size.Height);
                if (CheckSizeH(newlocation)) this.Size = new Size(Size.Width, newlocation.Height);

            }
        }

        #endregion


        private void label2_Click(object sender, EventArgs e)
        {
            //COUNT
            Count = 0;
            label5.Text = "0";
            
        }

        private void label3_Click(object sender, EventArgs e)
        {
            //LIST
            for (int i = 0; i < dataGridView1.Rows.Count - 1; i++)
            {

                dataGridView1.Rows[i].Visible = false;

            }
        }
        static public string Link = "";

        private void backgroundWorker2_DoWork(object sender, DoWorkEventArgs e)
        {
            while (!Connected)
            {
                var data = "";
                try
                {
                    string line;
                    try
                    {
                        System.IO.StreamReader file = new System.IO.StreamReader(Application.StartupPath + "\\path.txt");
                        while ((line = file.ReadLine()) != null)
                        {
                            Path = line;
                        }
                        
                        file.Close();
                    }
                    catch

                    {

                        Form2 f = new Form2();
                        f.ShowDialog();
                        var request = new RequestHelper();
                        var data1 = request.GetResponce(Path);
                        var idChateg = "";
                        var regex = new Regex("(?<=channelId=).*?(?=&)");
                        if (regex.IsMatch(data1)) idChateg = regex.Match(data1).ToString();

                        Path = "http://chat.sc2tv.ru/memfs/channel-" + idChateg + ".json";

                        System.IO.File.AppendAllText(Application.StartupPath + "\\path.txt", Path);

                    }

                    data = c.DownloadString(Path);
                    dataGridView1.Rows.Clear();
                    Count = 0;
                    label5.Text = Count.ToString();
                    TimerOn = true;
                    o = JObject.Parse(data);
                    Last = "" + o["messages"][0]["id"];
                    
                    Connected = true;
                }
                catch (WebException ex)
                {
                    dataGridView1.Rows.Clear();
                    if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                    {
                        var resp = (HttpWebResponse)ex.Response;
                        if (resp.StatusCode == HttpStatusCode.NotFound) // HTTP 404
                        {
                            try
                            {
                                if (dataGridView1.Rows[0].Cells[0].Value.ToString() != "No Connection")
                                {
                                    dataGridView1.Rows.Add("No Connection");
                                }
                            }
                            catch
                            {
                                try
                                {
                                    dataGridView1.Rows.Add("No Connection");
                                }
                                catch 
                                { 
                                }
                             
                            }
                        }



                    }
                    //throw any other exception - this should not occur
                }
                Thread.Sleep(1000);
            }
        }

        private void panel7_Paint(object sender, PaintEventArgs e)
        {

        }


        private void label2_MouseLeave(object sender, EventArgs e)
        {
            label2.ForeColor = Color.FromArgb(33, 33, 255);
        }

        private void label2_MouseEnter(object sender, EventArgs e)
        {
            label2.ForeColor = Color.FromArgb(33, 33, 200);
        }

        private void label3_MouseEnter(object sender, EventArgs e)
        {
            label3.ForeColor = Color.FromArgb(33, 33, 200);
        }

        private void label3_MouseLeave(object sender, EventArgs e)
        {
            label3.ForeColor = Color.FromArgb(33, 33, 255);
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            var data = "";

            try
            {
                data = c.DownloadString(Path);
                o = JObject.Parse(data);
                dataGridView1.Visible = true;


                string Current = "" + o["messages"][0]["id"];
                int Counter = -1;
                if (Current != Last)
                {
                    while (Current != Last)
                    {
                        Counter++;
                        Current = "" + o["messages"][Counter]["id"];
                        if (Current != Last)
                        {
                            Contain(o["messages"][Counter]["name"].ToString());
                            var regex1 = new Regex("http://(\\S+?)\\.(jpg|png|gif)");
                            if (regex1.IsMatch(o["messages"][Counter]["message"].ToString()) && Images)
                            {

                                Form3 f = new Form3();
                                if (this.WindowState == FormWindowState.Minimized)
                                    f.Show();
                                else
                                    f.Show(this);
                                f.GetLink(regex1.Match(o["messages"][Counter]["message"].ToString()).ToString());
                                f.Opacity = 100;
                            }
                        }
                    }
                }
                Last = "" + o["messages"][0]["id"];




            }
            catch (WebException ex)
            {
                if (ex.Status == WebExceptionStatus.ProtocolError && ex.Response != null)
                {
                    var resp = (HttpWebResponse)ex.Response;
                    if (resp.StatusCode == HttpStatusCode.NotFound) // HTTP 404
                    {

                    }
                }
                //throw any other exception - this should not occur

            }

            catch (JsonReaderException)
            {

            }
            
        }

        private void label6_Click(object sender, EventArgs e)
        {
            if (Images)
            {
                Images = false;
                label6.ForeColor = Color.FromArgb(56,56,56);
            }
            else
            {
                Images = true;
                label6.ForeColor = Color.FromArgb(33,33,255);
            }

        }

        bool Images = true;
        Size size = new Size();

        private void label7_Click_1(object sender, EventArgs e)
        {
            size = this.Size;
            this.Size = new Size(0, 0);
        }

        private void label7_MouseEnter(object sender, EventArgs e)
        {
            panel5.BackColor = Color.FromArgb(36,36,36);
        }

        private void label7_MouseLeave(object sender, EventArgs e)
        {
            panel5.BackColor = Color.FromArgb(46, 46, 46);
        }

        private void notifyIcon1_MouseClick(object sender, MouseEventArgs e)
        {
            if (size != new Size(0,0))
            this.Size = size;
            this.Activate();
        }

        private void label6_MouseEnter(object sender, EventArgs e)
        {
            if (Images)
                label6.ForeColor = Color.FromArgb(33, 33, 220);
            else
                label6.ForeColor = Color.FromArgb(76, 76, 76);
        }

        private void label6_MouseLeave(object sender, EventArgs e)
        {
            if (Images)
                label6.ForeColor = Color.FromArgb(33, 33, 255);
            else
                label6.ForeColor = Color.FromArgb(56, 56, 56);
        }
        static public bool Channel = false;
        private void label8_Click(object sender, EventArgs e)
        {
            
            Form4 f4 = new Form4();
            f4.ShowDialog();
            if (Channel)
            {
                Channel = false;
                timer1.Enabled = false;
                timer2.Enabled = true;
                Connected = false;
                try
                {
                    System.IO.File.Delete(Application.StartupPath + "\\path.txt");
                }
                catch
                {
                }
                if (!backgroundWorker2.IsBusy)
                    backgroundWorker2.RunWorkerAsync();
                else if (!backgroundWorker2.CancellationPending)
                    backgroundWorker2.CancelAsync();
            }
            
        }

        bool TimerOn = false;
        private void timer2_Tick(object sender, EventArgs e)
        {
            if (TimerOn)
            {
                timer1.Enabled = true;
                timer2.Enabled = false;
            }
        }

        private void label8_MouseEnter(object sender, EventArgs e)
        {
            label8.ForeColor = Color.FromArgb(33, 33, 220);
        }
        
        private void label8_MouseLeave(object sender, EventArgs e)
        {
            label8.ForeColor = Color.FromArgb(33, 33, 255);
        }


    }
    public class RequestHelper
    {
        public CookieContainer Cookie { get; set; }
        public bool IsReplaceCookie { get; set; }

        public string GetResponce(string url, string proxy = "", string userAgent = "Mozilla/5.0 (Windows NT 6.3; WOW64; Trident/7.0; rv:11.0) like Gecko",
            int timeout = 10000, string referal = "", string method = "GET",
            string host = "", byte[] postData = null, string contentType = "application/x-www-form-urlencoded; charset=UTF-8", string accept = "text/html, application/xhtml+xml, */*")
        {
            try
            {
                var reqGetInfo = (HttpWebRequest)WebRequest.Create(url);

                if (proxy.Length > 0) reqGetInfo.Proxy = new WebProxy(proxy);
                if (userAgent.Length > 0) reqGetInfo.UserAgent = userAgent;
                if (referal.Length > 0) reqGetInfo.Referer = referal;
                if (host.Length > 0) reqGetInfo.Host = host;

                reqGetInfo.ContentType = contentType;
                reqGetInfo.Headers.Add("Accept-Language", "ru,en-US;q=0.7,en;q=0.3");
                reqGetInfo.Method = method;
                reqGetInfo.Accept = accept;
                reqGetInfo.KeepAlive = true;
                reqGetInfo.AllowAutoRedirect = true;
                reqGetInfo.CookieContainer = Cookie ?? new CookieContainer();

                if (postData != null)
                {
                    reqGetInfo.ContentLength = postData.Length;
                    var stream = reqGetInfo.GetRequestStream();
                    stream.Write(postData, 0, postData.Length);
                    stream.Close();
                }
                else
                {
                    reqGetInfo.ContentLength = 0;
                }


                var respInfo = (HttpWebResponse)reqGetInfo.GetResponse();
                {
                    if (Cookie == null || IsReplaceCookie == true)
                    {
                        if (Cookie == null) Cookie = new CookieContainer();
                        Cookie.Add(respInfo.Cookies);
                    }

                    using (var data = respInfo.GetResponseStream())
                    {

                        var reader = new StreamReader(data);
                        return reader.ReadToEnd();
                    }
                }
            }
            catch (Exception ex)
            {
                var mes = ex.Message;
                return "-1";
            }
        }
    }
}
