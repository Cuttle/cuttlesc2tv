﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdrenalineNew
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>

        static bool IsSingleInstance()
        {
            try
            {
                //Проверяем на наличие мутекса в системе
                Mutex.OpenExisting("MY_UNIQUE_MUTEX_NAME");
            }
            catch
            {
                //Если получили исключение значит такого мутекса нет, и его нужно создать
                Mutex mutex = new Mutex(true, "MY_UNIQUE_MUTEX_NAME");
                return true;
            }
            //Если исключения не было, то процесс с таким мутексом уже запущен
            return false;
        }


        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main()
        {
            if (IsSingleInstance())
            {
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new AdrenalineNew());



            }
        }
    }
}
