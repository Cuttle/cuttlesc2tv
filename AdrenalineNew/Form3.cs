﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AdrenalineNew
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            TopMost = true;
            InitializeComponent();
            backgroundWorker1.RunWorkerAsync();
        }



        [DllImport("user32")]
        public static extern void SendMessageA(IntPtr hWnd, int wMsg, int wParam, int lParam);
        [DllImport("user32")]
        public static extern void ReleaseCapture();

        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HTCAPTION = 2;



        private void Form1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessageA(this.Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }

        private void pictureBox1_Paint(object sender, PaintEventArgs e)
        {

            pictureBox1.SizeMode = PictureBoxSizeMode.AutoSize;
            this.Width = pictureBox1.Width;
            this.Height = pictureBox1.Height;
            pictureBox1.MaximumSize = new Size(1024, 768);
            if (pictureBox1.Width == 768 || pictureBox1.Height == 1024)
                pictureBox1.SizeMode = PictureBoxSizeMode.StretchImage;

        }

        private void pictureBox1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                ReleaseCapture();
                SendMessageA(this.Handle, WM_NCLBUTTONDOWN, HTCAPTION, 0);
            }
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            while (true)
            {
                Thread.Sleep(15000);
                this.Close();
            }
        }

        public void GetLink(string s)
        {
            pictureBox1.Load(s);
        }

        private void Form3_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

        private void pictureBox1_DoubleClick(object sender, EventArgs e)
        {
            this.Close();
        }

    }


}
